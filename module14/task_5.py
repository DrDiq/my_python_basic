def get_gcd(a):
    gcd = 1
    for i in range(1, a + 1):
        if a % i == 0:
            gcd = i
        if gcd > 1:
            break
    return gcd


num = int(input('Введите число: '))
if num > 1:
    gsd = get_gcd(num)
    print('Наименьший делитель, отличный от единицы:', gsd)
else:
    print('Ошибка ввода.\nЧисло должно быть больще 1-го.')