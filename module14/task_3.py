def amount_num(num):
    temp_num = num
    first_num = 0
    second_num = num
    amount = 0
    actions = 0
    while temp_num > 1:
        temp_num /= 10
        actions += 1
    for i in range (actions):
        first_num = second_num % 10
        second_num //= 10
        amount += first_num
        first_num = 0
    print('\nСумма чисел:', amount)
    return amount

def count_num(num):
    qty_num = 0
    while num > 1:
        num /= 10
        qty_num += 1
    print('Количество цифр в числе:', qty_num)
    return qty_num


num = int(input('Введите число: '))
first_num = amount_num(num)
second_num = count_num(num)
print('Разность суммы и количества цифр:', first_num - second_num)

