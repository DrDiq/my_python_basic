import math

def reverse_whole_part(number):
    whole_part = math.floor(number)
    temp_num = whole_part
    actions = 1
    num = whole_part
    first_divider = 10
    reverse_num = 0

    while temp_num > 1:
        temp_num //= 10
        actions += 1

    temp = actions
    for i in range(1, temp + 1):
        temp_num = num % first_divider ** i
        temp_num //= first_divider ** (i - 1)
        temp_num *= first_divider ** (temp - 1)
        reverse_num += temp_num
        temp_num = whole_part
        temp -= 1

    return reverse_num

def reverse_material_part(num):
    temp_num = num
    actions = 0
    first_divider = 10
    reverse_num = 0

    while temp_num % 1 != 0:
        actions += 1
        temp_num *= 10
    if actions == 13:
        actions = 2

    number = round(num % 1, actions)
    number *= 10 ** actions
    temp_num = math.floor(number)
    num = temp_num

    temp = actions
    for i in range(1, temp + 1):
        temp_num = num % first_divider ** i
        temp_num //= first_divider ** (i - 1)
        temp_num *= first_divider ** (temp - 1)
        reverse_num += temp_num
        temp_num = temp
        temp -= 1

    reverse_num /= 10 ** actions

    return reverse_num

first_num = float(input('Введите первое число: '))

second_num = float(input('Введите второе число: '))

first_whole_part = reverse_whole_part(first_num)
first_material_part = reverse_material_part(first_num)
first_reverse_num = first_whole_part + first_material_part
print('Первое число наоборот:', first_reverse_num)

second_whole_part = reverse_whole_part(second_num)
second_material_part = reverse_material_part(second_num)
second_reverse_num = second_whole_part + second_material_part
print('Второе число наоборот:', second_reverse_num)

print('Сумма:', first_reverse_num + second_reverse_num)



